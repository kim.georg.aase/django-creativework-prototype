from django.urls import include, path
from rest_framework import routers

# https://github.com/alanjds/drf-nested-routers
#from rest_framework_nested import routers

from . import views


class CustomBaseRouter(routers.BaseRouter):
    def __init__(self):
        super().__init__()
        self.register_urls = list()

    def get_default_basename(self, viewset):
        """
        If `basename` is not specified, attempt to automatically determine
        it from the viewset.
        """
        raise NotImplementedError('get_default_basename must be overridden')

    def get_urls(self):
        return self.register_urls


router2 = CustomBaseRouter()


router = routers.DefaultRouter()

# TODO: Should I hav deep url hierarchy as prefix, or should i keep it flat?
router_internal = routers.DefaultRouter()
router.register(r'internals/users', views.UserViewSet)
router.register(r'internals/groups', views.GroupViewSet)
router.registry.extend(router_internal.registry)

router.register(r'things/creative_works/books', views.BookViewSet)
router.register(r'things/creative_works/books/editions', views.BookEditionViewSet)
router.register(r'things/creative_works/books/editions/paperbacks', views.BookEditionPaperbackViewSet)

router.register(r'things/persons', views.PersonViewSet)
# router.register(r'contributors', views.ContributorViewSet)
router.register(r'things/organizations', views.OrganizationViewSet)
router.register(r'things/places', views.PlaceViewSet)

router.register(r'things/creative_works', views.CreativeWorkViewSet)
router.register(r'things', views.ThingViewSet)



urlpatterns = [
    path('api/', include(router.urls)),


    path('books/', views.BookListView.as_view(), name="view-book-list"),
    path('books/<int:pk>/', views.BookDetailView.as_view(), name="view-book-detail"),

    path('series/', views.TvSeriesListView.as_view(), name="view-series-list"),
]

"""path('persons/', views.PersonListView.as_view()),
    path('books/', views.BookListView.as_view()),
    path('books/<slug:slug>/', views.BookDetailView.as_view()),"""