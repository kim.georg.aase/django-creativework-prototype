from django.db import models
from django.conf import settings
import logging
from django.utils.translation import gettext_lazy as _
from django.urls import reverse
from django.db.models import Q
from django.core.exceptions import ValidationError
from django.contrib.contenttypes.models import ContentType
import langcodes

# https://django-polymorphic.readthedocs.io/en/stable/
from polymorphic.models import PolymorphicModel, PolymorphicManager

from creativework.model_helpers import CustomPolymorphicModelMixin

logger = logging.getLogger(__name__)

"""
    Only for testing.
    
    Main project wil contain more tables, fields, and logic.
    It wil also be splited in smaller multiple files in main project
    
    
    Actual DB tables so far:
        - Thing
        - Person
        - CreativeWork
        - Book
        - BookEdition
        - Part
        - Contributor
    The rest are just proxy models so... so far
        
        
    TODO: Hard to return real class instead of base class(Thing) in some cases without avoiding extra lokup reounds to db
        - Some cases Thing is good enough
        - Can some be done in the context setup?
        
    TODO: Fix save when setting parts
        
"""


########################################################################################################################


class Thing(CustomPolymorphicModelMixin, PolymorphicModel):
    id = models.BigAutoField(primary_key=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    creator = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, null=True, blank=True)

    name = models.TextField()
    disambiguation = models.TextField(blank=True, null=True)

    # polymorphic_ctype

    """def load_child(self, child):
        super().load_child(child)
        child.id = self.id
        child.created = self.created
        child.updated = self.updated
        child.creator = self.creator
        child.name = self.name
        child.disambiguation = self.disambiguation"""

    def save(self, **kwargs):
        instance = super().save(**kwargs)
        return instance

    def get_absolute_url(self):
        name = self._meta.verbose_name
        return reverse(f'view-{name}-detail', args=[str(self.id)])

    """def __str__(self):
        return f"{self.polymorphic_ctype.model} - {str(self.name)}"
    """

    def __str__(self):
        return str(self.name)

    def __unicode__(self):
        return self.__str__()


########################################################################################################################


class Place(Thing):
    # born = GenericRelation("Person", object_id_field="id", content_type_field='polymorphic_ctype_id')

    class Meta:
        proxy = True


class Organization(Thing):
    class Meta:
        proxy = True


########################################################################################################################


class Person(Thing):
    birth_date = models.DateField(blank=True, null=True)
    birth_place = models.ForeignKey(Thing, null=True, blank=True,
                                    on_delete=models.SET_NULL,
                                    related_name="birth_place_for")
    death_date = models.DateField(blank=True, null=True)
    death_place = models.ForeignKey(Thing, null=True, blank=True,
                                    on_delete=models.SET_NULL,
                                    related_name="death_place_for")
    burial_place = models.ForeignKey(Thing, null=True, blank=True,
                                     on_delete=models.SET_NULL,
                                     related_name="burial_place_for")

    @staticmethod
    def birth_place_limit_choices_to_models():
        return Place,

    @staticmethod
    def death_place_limit_choices_to_models():
        return Place,

    @staticmethod
    def burial_place_limit_choices_to_models():
        return Place,


########################################################################################################################


def validate_language(value):
    if value is None:
        return
    langcodes.Language.get(value).is_valid()
    if not langcodes.Language.get(value).is_valid():
        raise ValidationError(
            _('%(value)s is not a valid language'),
            params={'value': value},
        )


class CreativeWork(Thing):
    class CreativeWorkStatus(models.IntegerChoices):
        INCOMPLETE = 1, _('Incomplete')
        DRAFT = 2, _('Draft')
        PUBLISHED = 3, _('Published')
        OBSOLETE = 4, _('Obsolete')

    creative_work_status = models.IntegerField(
        choices=CreativeWorkStatus.choices,
        default=None,
        blank=True, null=True
    )

    # publish_date = models.DateField(blank=True, null=True)
    original_publish_date = models.DateField(blank=True, null=True)
    original_title = models.TextField(blank=True, null=True)

    original_language = models.TextField(null=True, blank=True,
                                         validators=[validate_language])
    language = models.TextField(null=True, blank=True,
                                validators=[validate_language])

    # contributors = models.ManyToManyField("self", through='Contributor', related_name="contributor_to")

    """def load_child(self, child):
        super().load_child(child)
        child.creative_work_status = self.creative_work_status
        child.original_publish_date = self.original_publish_date
        child.original_title = self.original_title
        child.original_language = self.original_language
        child.language = self.language"""

    def save(self, **kwargs):
        if self.language:
            lang = langcodes.Language.get(self.language)
            self.language = langcodes.standardize_tag(str(lang))
        if self.original_language:
            org_lang = langcodes.Language.get(self.original_language)
            self.original_language = langcodes.standardize_tag(str(org_lang))
        instance = super().save(**kwargs)
        return instance


########################################################################################################################


class Movie(CreativeWork):
    class Meta:
        proxy = True


########################################################################################################################


class PartManager(PolymorphicManager):

    def get_items(self, part_class, item_class, part_property, item_property=None, reload=False):

        parts = self.all()
        parts_filtered = [part for part in parts
                          if part_class is None or part.get_real_instance_class() == part_class]

        if item_property is None:
            item_property = part_property

        items = [getattr(part, part_property)
                 for part in parts_filtered
                 if getattr(part, item_property, None) is not None
                 and getattr(part, item_property).get_real_instance_class() == item_class]
        return items  # Return base classes (Thing) and not actual classes

    def get_item(self, part_class, item_class, part_property, item_property=None, reload=False):
        value = self.get_items(
            part_class=part_class,
            item_class=item_class,
            part_property=part_property,
            item_property=item_property,
            reload=reload)
        if value:
            return value[0]


class Part(CustomPolymorphicModelMixin, PolymorphicModel):
    is_part_of = models.ForeignKey(Thing, null=True, blank=True,
                                   on_delete=models.PROTECT,
                                   related_name="has_parts",
                                   related_query_name="has_part")
    has_part = models.ForeignKey(Thing, null=True, blank=True,
                                 on_delete=models.PROTECT,
                                 related_name="is_part_of",
                                 related_query_name="is_part_of")

    order = models.FloatField(null=True, blank=True, default=None)  # Specified order (lowest first, highest last)
    weight = models.FloatField(null=True, blank=True, default=None)  # Calculated weight (highest first, lowest last)

    objects = PartManager()


########################################################################################################################

"""
def get_has_part_items(instance, name, part_class, item_class=None, part_property="has_parts", reload=False):
    field_name = f"_cached_{name}"  # TODO: find a better name. _{something telling what it is}_{field_name}
    if not reload and hasattr(instance, field_name):
        return getattr(instance, field_name)
    values = [getattr(x, part_property)
              for x in instance.has_parts.filter(Q(instance_of=part_class))
              if item_class is not None and isinstance(getattr(x, part_property), item_class)]
    setattr(instance, field_name, values)
    return values


def get_is_part_of_items(instance, name, part_class, item_class=None, part_property="is_part_of", reload=False):
    field_name = f"_cached_{name}"  # TODO: find a better name. _{something telling what it is}_{field_name}
    if not reload and hasattr(instance, field_name):
        return getattr(instance, field_name)
    values = [getattr(x, part_property)
              for x in instance.is_part_of.filter(Q(instance_of=part_class))
              if item_class is not None and isinstance(getattr(x, part_property), item_class)]
    setattr(instance, field_name, values)
    return values


def get_is_part_of_item(instance, name, part_class, item_class=None, part_property="is_part_of"):
    value = get_is_part_of_items(
        instance=instance,
        name=name,
        part_class=part_class,
        item_class=item_class,
        part_property=part_property)
    if value:
        return value[0]
"""


class TvSeries(CreativeWork):
    class Meta:
        proxy = True

    @property
    def seasons(self):
        return self.get_helper_from_func("episodes", SeasonPart.objects.get_seasons)(self.has_parts)

    @property
    def episodes(self):
        return self.get_helper_from_func("episodes", EpisodePart.objects.get_episodes)(self.has_parts)


class TvSeason(CreativeWork):
    class Meta:
        proxy = True

    @property
    def series(self):
        return self.get_helper_from_func("series", SeasonPart.objects.get_series)(self.is_part_of)

    @series.setter
    def series(self, value):
        # TODO: Should i add posibility to add, then save it when save on this is called????
        pass

    @property
    def episodes(self):
        ret = self.get_helper_from_func("episodes", EpisodePart.objects.get_episodes)(self.has_parts)
        return ret

    """@episodes.setter
    def episodes(self, value):
        pass"""


class TvEpisode(CreativeWork):
    class Meta:
        proxy = True

    @property
    def series(self):
        return self.get_helper_from_func("series", EpisodePart.objects.get_series)(self.is_part_of)

    @series.setter
    def series(self, value):
        self.add_helper(name="series", value=value, valid_types=[TvSeries, ])

    @property
    def series_order(self):
        return self.get_helper_from_func("series_order", EpisodePart.objects.get_series_order)(self.is_part_of)

    @series_order.setter
    def series_order(self, value):
        self.add_helper(name="series_order", value=value, valid_types=[float, ])

    @property
    def season(self):
        return self.get_helper_from_func("season", EpisodePart.objects.get_season)(self.is_part_of)

    @season.setter
    def season(self, value):
        self.add_helper(name="season", value=value, valid_types=[TvSeason, ])

    @property
    def season_order(self):
        return self.get_helper_from_func("season_order", EpisodePart.objects.get_season_order)(self.is_part_of)

    @season_order.setter
    def season_order(self, value):
        self.add_helper(name="season_order", value=value, valid_types=[float, ])

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)

        series_field_name = "_cached_series"
        series_order_field_name = "_cached_series_order"
        if hasattr(self, f"{series_field_name}_is_set") or hasattr(self, f"{series_order_field_name}_is_set"):
            # TODO: Extract. Gona be almost duplicated a lot
            links = [x for x in self.is_part_of.filter(Q(instance_of=EpisodePart)) if isinstance(x.series, TvSeries)]
            if links:
                link = links[0]
                if self.series:
                    if link.is_part_of.id != self.series.id or link.order != self.series_order:
                        link.is_part_of = self.series
                        link.order = self.series_order
                        link.save()
                else:
                    link.delete()
            elif self.series:
                EpisodePart.objects.create(has_part=self, is_part_of=self.series, order=self.series_order)

        season_field_name = "_cached_season"
        season_order_field_name = "_cached_season_order"
        if hasattr(self, f"{season_field_name}_is_set") or hasattr(self, f"{season_order_field_name}_is_set"):
            links = [x for x in self.is_part_of.filter(Q(instance_of=EpisodePart)) if isinstance(x.season, TvSeason)]
            if links:
                link = links[0]
                if self.season:
                    if link.is_part_of.id != self.season.id or link.order != self.season_order:
                        link.is_part_of = self.season
                        link.order = self.season_order
                        link.save()
                else:
                    link.delete()
            elif self.season:
                EpisodePart.objects.create(has_part=self, is_part_of=self.season, order=self.season_order)


class SeasonPartManager(PartManager):

    def get_series(self, queryset=None):
        if queryset is None:
            queryset = self
        return queryset.get_item(part_class=SeasonPart, item_class=TvSeries, part_property="series")

    def get_series_order(self, queryset=None):
        if queryset is None:
            queryset = self
        return queryset.get_item(
            part_class=SeasonPart, item_class=TvSeries, part_property="order", item_property="series")

    def get_seasons(self, queryset=None):
        if queryset is None:
            queryset = self
        return queryset.get_items(part_class=SeasonPart, item_class=TvSeason, part_property="season")


class SeasonPart(Part):
    objects = SeasonPartManager()

    class Meta:
        proxy = True

    @property
    def series(self):
        if self.is_part_of.get_real_instance_class() == TvSeries:
            return self.is_part_of
        return None

    @series.setter
    def series(self, value):
        pass

    @property
    def season(self):
        if self.has_part.get_real_instance_class() == TvSeason:
            return self.has_part
        return None

    @season.setter
    def season(self, value):
        pass

    @staticmethod
    def has_part_limit_choices_to_models():
        return TvSeason,

    @staticmethod
    def is_part_of_limit_choices_to_models():
        return TvSeries,


class EpisodePartManager(SeasonPartManager):

    def get_series(self, queryset=None):
        if queryset is None:
            queryset = self
        return queryset.get_item(part_class=EpisodePart, item_class=TvSeries, part_property="series")

    def get_series_order(self, queryset=None):
        if queryset is None:
            queryset = self
        return queryset.get_item(
            part_class=EpisodePart, item_class=TvSeries, part_property="order", item_property="series")

    def get_season(self, queryset=None):
        if queryset is None:
            queryset = self
        return queryset.get_item(part_class=EpisodePart, item_class=TvSeason, part_property="season")

    def get_season_order(self, queryset=None):
        if queryset is None:
            queryset = self
        return queryset.get_item(
            part_class=EpisodePart, item_class=TvSeason, part_property="order", item_property="season")

    def get_episodes(self, queryset=None):
        if queryset is None:
            queryset = self
        return queryset.get_items(part_class=EpisodePart, item_class=TvEpisode, part_property="episode")


class EpisodePart(Part):
    objects = EpisodePartManager()

    class Meta:
        proxy = True

    @property
    def series(self):
        if self.is_part_of.get_real_instance_class() == TvSeries:
            return self.is_part_of
        return None

    @series.setter
    def series(self, value):
        pass

    @property
    def season(self):
        if self.is_part_of.get_real_instance_class() == TvSeason:
            return self.is_part_of
        return None

    @season.setter
    def season(self, value):
        pass

    @property
    def episode(self):
        if self.has_part.get_real_instance_class() == TvEpisode:
            return self.has_part
        return None

    @episode.setter
    def episode(self, value):
        pass

    @staticmethod
    def has_part_limit_choices_to_models():
        return TvEpisode,

    @staticmethod
    def is_part_of_limit_choices_to_models():
        return TvSeries, TvSeason


########################################################################################################################


class Book(CreativeWork):
    pass

    def editions_limited(self):
        return self.editions.all()[:2]


##########

class BookEdition(CreativeWork):
    # TODO: Split out formats types (text, audio, digitaltext?)

    # book = models.ForeignKey(Book, blank=True, null=True)
    book_parent = models.ForeignKey(Thing, null=True, blank=True,
                                    on_delete=models.SET_NULL,
                                    related_name="editions")

    abridged = models.BooleanField(blank=True, null=True)

    duration = models.IntegerField(blank=True, null=True)
    pages = models.IntegerField(blank=True, null=True)

    # TODO: Should some of thise (asin) be moved into the ExternalReference table
    isbn = models.CharField(max_length=10, blank=True, null=True)
    isbn13 = models.CharField(max_length=13, blank=True, null=True)
    asin = models.CharField(max_length=10, blank=True, null=True)

    @staticmethod
    def book_parent_limit_choices_to_models():
        return Book,


##########


class BookTextEdition(BookEdition):
    # TODO: Split out in seperate table? (pages)

    class Meta:
        proxy = True


class BookDigitalTextEdition(BookEdition):
    # TODO: Split out in seperate table? (size + pages)

    class Meta:
        proxy = True


class BookAudioEdition(BookEdition):
    # TODO: Split out in seperate table? (duration)

    class Meta:
        proxy = True


##########


class BookEditionEbook(BookDigitalTextEdition):
    class Meta:
        proxy = True


class BookEditionPaperback(BookTextEdition):
    class Meta:
        proxy = True
        verbose_name = "paperback"


class BookEditionAudible(BookAudioEdition):
    class Meta:
        proxy = True
        verbose_name = "audible"


class BookEditionKindle(BookDigitalTextEdition):
    class Meta:
        proxy = True


class BookEditionHardcover(BookTextEdition):
    class Meta:
        proxy = True
        verbose_name = "hardcover"


class BookEditionAudiobook(BookAudioEdition):
    class Meta:
        proxy = True


class BookEditionAudioCD(BookAudioEdition):
    class Meta:
        proxy = True


class BookEditionMassMarketPaperback(BookTextEdition):
    class Meta:
        proxy = True


########################################################################################################################
########################################################################################################################
########################################################################################################################


"""class ThingRelation(PolymorphicModel):
    order = models.FloatField(null=True, blank=True, default=None)  # Specified order (lowest first, highest last)
    weight = models.FloatField(null=True, blank=True, default=None)  # Calculated weight (highest first, lowest last)"""

"""class ContributionType(models.IntegerChoices):
    # CONTRIBUTOR = 0, _('Contributor')
    AUTHOR = 1, _('Author')
    TRANSLATOR = 2, _('Translator')
    NARRATOR = 3, _('Narrator')
    # ILLUSTRATOR = 4, _('Illustrator')
    # EDITOR = 5, _('Editor')
    # COPYRIGHT_HOLDER = 6, _('Copyright Holder')
    # CREATOR = 7, _('Creator')
    # FUNDER = 8, _('Funder')
    # Maintainer = 9, _('Maintainer')
    # PRODUCER = 10, _('Producer')
    # PROVIDER = 11, _('Provider')
    # PUBLISHER = 12, _('Publisher')
    # SPONSOR = 13, _('Sponsor')
    # CHARACTER = 14, _('character')
    # ACCOUNTABLE_PERSON = 15, _('Accountable Person')
    # ACTOR = 16, _('Actor')
    # MUSIC = 17, _('Music')
    # DIRECTOR = 18, _('Director')
"""

CREATIVE_WORK_TYPES = [Book, BookEdition]
CONTRIBUTOR_TYPES = [Person, Organization]


def _get_written_work_contributors():
    return [Book, ] + BookEdition.get_model_descendants_and_self()


def _get_moving_picture_work_contributors():
    return set(Movie.get_model_descendants_and_self()
               + TvSeries.get_model_descendants_and_self()
               + TvSeason.get_model_descendants_and_self()
               + TvEpisode.get_model_descendants_and_self())


class Contributor(CustomPolymorphicModelMixin, PolymorphicModel):
    work = models.ForeignKey(Thing, null=True, blank=True,
                             on_delete=models.PROTECT,
                             related_name="contributors",
                             related_query_name="contributor")
    contributor = models.ForeignKey(Thing, null=True, blank=True,
                                    on_delete=models.PROTECT,
                                    related_name="contributions",
                                    related_query_name="contribution")

    # contribution_type = models.IntegerField(choices=ContributionType.choices, blank=True, null=True, default=None)
    # contribution_type_disambiguation = models.TextField(blank=True, null=True)
    main_contributor = models.BooleanField(blank=True, null=True)
    order = models.FloatField(null=True, blank=True, default=None)  # Specified order (lowest first, highest last)
    weight = models.FloatField(null=True, blank=True, default=None)  # Calculated weight (highest first, lowest last)

    @staticmethod
    def contributor_limit_choices_to_models():
        # field_name + _limit_choices_to_models is used to automaticaly get limit_choices_to
        #   Was not able to dynamicaly add limit_choices_to in FK. Had to direcly list the values
        # This also solvle the problem where child classes need diferent limits than parent
        return CONTRIBUTOR_TYPES

    @staticmethod
    def work_limit_choices_to_models():
        return CreativeWork.get_model_descendants_and_self()

    """def clean(self):
        super().clean()
        if self.contribution_type is None and self.contribution_type_disambiguation is None:
            raise ValidationError("contribution_type and contribution_type_disambiguation cant both be null")"""


class ContributorAuthor(Contributor):
    class Meta:
        proxy = True
        verbose_name = "author"

    @staticmethod
    def work_limit_choices_to_models():
        return _get_written_work_contributors()


class ContributorTranslator(Contributor):
    class Meta:
        proxy = True
        verbose_name = "translator"

    @staticmethod
    def work_limit_choices_to_models():
        return _get_written_work_contributors()


class ContributorNarrator(Contributor):
    class Meta:
        proxy = True
        verbose_name = "narrator"

    @staticmethod
    def work_limit_choices_to_models():
        return _get_written_work_contributors()


class ContributorActor(Contributor):
    class Meta:
        proxy = True
        verbose_name = "actor"

    @staticmethod
    def work_limit_choices_to_models():
        return _get_moving_picture_work_contributors()


class ContributorDirector(Contributor):
    class Meta:
        proxy = True
        verbose_name = "director"

    @staticmethod
    def work_limit_choices_to_models():
        return _get_moving_picture_work_contributors()
