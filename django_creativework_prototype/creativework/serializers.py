from django.contrib.auth.models import User, Group
from rest_framework import serializers
from rest_framework.settings import api_settings

# https://github.com/apirobot/django-rest-polymorphic
from rest_polymorphic.serializers import PolymorphicSerializer

# https://github.com/alanjds/drf-nested-routers
# from rest_framework_nested.relations import NestedHyperlinkedRelatedField

from .model_helpers import CustomPolymorphicModelMixin
from .models import Thing
from .models import CreativeWork
from .models import (
    Person,
    Organization,
    Place
)
from .models import (
    Book,
    BookEdition
)
from .models import (
    BookTextEdition,
    BookAudioEdition,
    BookDigitalTextEdition
)
from .models import (
    BookEditionEbook,
    BookEditionKindle,
    BookEditionAudible,
    BookEditionHardcover,
    BookEditionPaperback,
    BookEditionAudioCD,
    BookEditionMassMarketPaperback,
    BookEditionAudiobook
)
from .models import (
    Contributor,
    ContributorAuthor,
    ContributorNarrator,
    ContributorTranslator
)


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email']


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']


########################################################################################################################
########################################################################################################################
########################################################################################################################


class PolymorphicFieldMixin(metaclass=serializers.SerializerMetaclass):
    polymorphic_ctype = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        # exclude = ('polymorphic_ctype')
        fields = tuple()


########################################################################################################################


class ThingSerializer(PolymorphicFieldMixin, serializers.ModelSerializer):
    creator = serializers.StringRelatedField(
        default=serializers.CurrentUserDefault(),
        read_only=True
    )

    class Meta:
        model = Thing
        fields = PolymorphicFieldMixin.Meta.fields + \
                 ("url", "id", "creator", "name", "created", "updated", "disambiguation")

    def get_queryset(self):
        return ThingSerializer.Meta.model.objects.all()


class ThingListSerializer(PolymorphicFieldMixin, serializers.ModelSerializer):
    class Meta:
        model = Thing
        fields = PolymorphicFieldMixin.Meta.fields + ("url", "id", "name")

    def get_queryset(self):
        return ThingSerializer.Meta.model.objects.all()


########################################################################################################################


class OrganizationSerializer(ThingSerializer):
    class Meta:
        model = Organization
        fields = ThingSerializer.Meta.fields


class PlaceSerializer(ThingSerializer):
    class Meta:
        model = Place
        fields = ThingSerializer.Meta.fields


########################################################################################################################


class PersonSerializer(ThingSerializer):
    birth_place = serializers.SerializerMethodField()  # Get nested data instead of only id link

    # death_place = serializers.PrimaryKeyRelatedField(queryset=Place.objects.all())

    """def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        def limit_choices_to(field_name):
            fld = self.fields[field_name]
            fld.queryset = Place.objects.queryset.all()

        limit_choices_to('death_place')"""

    class Meta:
        model = Person
        fields = ThingSerializer.Meta.fields + \
                 ("birth_date", "birth_place", "death_date", "death_place", "burial_place")

    def get_birth_place(self, obj):
        instance = obj.birth_place
        place_serializer = ThingPolymorphicSerializer(instance, context=self.context)
        data = place_serializer.data
        return dict(
            url=data["url"],
            id=data["id"],
            name=data["name"]
        )


class PersonEditSerializer(ThingSerializer):
    # Custom serializer for edition
    class Meta:
        model = Person
        fields = PersonSerializer.Meta.fields


########################################################################################################################

class CreativeWorkSerializer(ThingSerializer):
    class Meta:
        model = CreativeWork
        fields = ThingSerializer.Meta.fields + \
                 ("creative_work_status", "original_publish_date",
                  "original_title", "original_language", "language")


class CreativeWorkListSerializer(ThingListSerializer):
    class Meta:
        model = CreativeWork
        fields = ThingListSerializer.Meta.fields


########################################################################################################################

class BookEditionListSerializer(CreativeWorkListSerializer):
    class Meta:
        model = BookEdition
        fields = CreativeWorkListSerializer.Meta.fields


class BookSerializer(CreativeWorkSerializer):

    # List of eager loaded subset (nott all) + link to endpoint to get all editions
    editions_eager = BookEditionListSerializer(many=True, read_only=True, source='editions_limited')
    editions = serializers.HyperlinkedIdentityField( many=False, read_only=True, view_name='book-editions')

    class Meta:
        model = Book
        fields = CreativeWorkSerializer.Meta.fields + ('editions','editions_eager')


class BookListSerializer(CreativeWorkListSerializer):
    class Meta:
        model = Book
        fields = CreativeWorkListSerializer.Meta.fields


class BookEditionSerializer(CreativeWorkSerializer):
    class Meta:
        model = BookEdition
        fields = CreativeWorkSerializer.Meta.fields


########################################################################################################################


"""class BookEditionEbookSerializer(BookEditionSerializer):
    class Meta:
        model = BookEditionEbook
        fields = '__all__'


class BookEditionKindleSerializer(BookEditionSerializer):
    class Meta:
        model = BookEditionKindle
        fields = '__all__'


class BookEditionAudibleSerializer(BookEditionSerializer):
    class Meta:
        model = BookEditionAudible
        fields = '__all__'


class BookEditionHardcoverSerializer(BookEditionSerializer):
    class Meta:
        model = BookEditionHardcover
        fields = '__all__'
"""


class BookEditionPaperbackSerializer(BookEditionSerializer):
    class Meta:
        model = BookEditionPaperback
        fields = '__all__'


"""class BookEditionAudioCDSerializer(BookEditionSerializer):
    class Meta:
        model = BookEditionAudioCD
        fields = '__all__'


class BookEditionMassMarketPaperbackSerializer(BookEditionSerializer):
    class Meta:
        model = BookEditionMassMarketPaperback
        fields = '__all__'


class BookEditionAudiobookSerializer(BookEditionSerializer):
    class Meta:
        model = BookEditionAudiobook
        fields = '__all__'

"""


########################################################################################################################
########################################################################################################################


class BookEditionPolymorphicSerializer(PolymorphicSerializer):
    model_serializer_mapping = {
        BookEdition: BookEditionSerializer,
        BookEditionPaperback: BookEditionPaperbackSerializer
    }


class CreativeWorkPolymorphicSerializer(PolymorphicSerializer):
    model_serializer_mapping = {
                                   CreativeWork: CreativeWorkSerializer,
                                   Book: BookSerializer,
                               } | BookEditionPolymorphicSerializer.model_serializer_mapping


class CreativeWorkListPolymorphicSerializer(PolymorphicSerializer):
    model_serializer_mapping = {
                                   CreativeWork: CreativeWorkListSerializer,
                                   Book: BookListSerializer,
                               } | BookEditionPolymorphicSerializer.model_serializer_mapping


class ThingPolymorphicSerializer(PolymorphicSerializer):
    model_serializer_mapping = {
                                   Thing: ThingSerializer,
                                   Place: PlaceSerializer,
                                   Organization: OrganizationSerializer,
                                   Person: PersonSerializer,
                               } | CreativeWorkPolymorphicSerializer.model_serializer_mapping


class ThingListPolymorphicSerializer(PolymorphicSerializer):
    model_serializer_mapping = {
                                   Thing: ThingListSerializer,
                                   Place: PlaceSerializer,
                                   Organization: OrganizationSerializer,
                                   Person: PersonSerializer,
                               } | CreativeWorkListPolymorphicSerializer.model_serializer_mapping
