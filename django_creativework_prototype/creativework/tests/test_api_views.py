# tests/test_views.py
from django.test import TestCase
from rest_framework.test import APITestCase, URLPatternsTestCase
from django.urls import reverse
from rest_framework import status
from django.urls import include, path, reverse

from .factories import PersonFactory, UserFactory, SuperUserFactory

from ..models import Person
from ..urls import router


class PersonViewSetTestCase(APITestCase):
    urlpatterns = [
        path('', include(router.urls)),
    ]

    def setUp(self):
        self.user = SuperUserFactory(email='testuser@example.com')
        self.user.set_password('testpassword')
        self.user.save()
        self.client.login(email=self.user.email, password='testpassword')
        self.client.force_authenticate(user=self.user)
        self.list_url = reverse('person-list')

    def get_detail_url(self, person_id):
        return reverse("person-detail", kwargs={'pk': person_id})

    def test_get_list(self):
        people = [PersonFactory() for _ in range(0, 3)]

        response = self.client.get(self.list_url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            set(person['id'] for person in response.data['results']),
            set(person.id for person in people)
        )

    def test_get_detail(self):
        person = PersonFactory()
        response = self.client.get(self.get_detail_url(person.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['name'], person.name)

    def test_post(self):
        data = {
            'name': 'Test Name',
            'disambiguation': 'Test disambiguation',
        }
        self.assertEqual(Person.objects.count(), 0)
        response = self.client.post(self.list_url, data=data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Person.objects.count(), 1)
        person = Person.objects.all().first()
        for field_name in data.keys():
            self.assertEqual(getattr(person, field_name), data[field_name])

    def test_put(self):
        person = PersonFactory()
        data = {
            'name': 'new Name',
            'disambiguation': 'Test disambiguation',
        }
        response = self.client.put(
            self.get_detail_url(person.id),
            data=data
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # The object has really been updated
        person.refresh_from_db()
        for field_name in data.keys():
            self.assertEqual(getattr(person, field_name), data[field_name])

    def test_patch(self):
        person = PersonFactory()
        data = {'name': 'New name'}
        response = self.client.patch(
            self.get_detail_url(person.id),
            data=data
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # The object has really been updated
        person.refresh_from_db()
        self.assertEqual(person.name, data['name'])

    def test_delete(self):
        person = PersonFactory()
        response = self.client.get(self.get_detail_url(person.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.delete(self.get_detail_url(person.id))
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        response = self.client.get(self.get_detail_url(person.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_unauthenticated(self):
        self.client.logout()
        person = PersonFactory()

        with self.subTest('GET list page'):
            response = self.client.get(self.list_url)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        with self.subTest('GET detail page'):
            response = self.client.get(self.get_detail_url(person.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        with self.subTest('PUT'):
            data = {
                'name': 'New name',
                'disambiguation': 'New disambiguation',
            }
            response = self.client.put(self.get_detail_url(person.id), data=data)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
            person.refresh_from_db()
            self.assertNotEqual(person.name, data['name'])

        with self.subTest('PATCH'):
            data = {'name': 'New name'}
            response = self.client.patch(self.get_detail_url(person.id), data=data)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
            person.refresh_from_db()
            self.assertNotEqual(person.name, data['name'])

        with self.subTest('POST'):
            data = {
                'name': 'New name',
                'disambiguation': 'New disambiguation',
            }
        response = self.client.put(self.list_url, data=data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        with self.subTest('DELETE'):
            response = self.client.delete(self.get_detail_url(person.id))
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
            self.assertTrue(Person.objects.filter(id=person.id).exists())
