from django.test import TestCase

from rest_framework.request import Request
from rest_framework.test import APIRequestFactory

from ..serializers import PersonSerializer
from .factories import PersonFactory


def get_default_request():
    factory = APIRequestFactory()
    request = factory.get('/')
    serializer_context = {
        'request': Request(request),
    }
    return serializer_context


class ParsonSerializer(TestCase):
    def test_model_fields_name_only(self):
        person = PersonFactory()
        serializer = PersonSerializer(instance=person, context=get_default_request())
        self.assertEqual(
            serializer.data["name"],
            getattr(person, "name")
        )
