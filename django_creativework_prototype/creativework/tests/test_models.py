from django.test import TestCase
from django.contrib.contenttypes.models import ContentType
from .factories import PersonFactory
from ..models import Thing, Person


class PersonTestCase(TestCase):
    def test_str(self):
        """Test for string representation."""
        person = PersonFactory()
        self.assertEqual(str(person), person.name)
