from django.contrib.auth.models import User

# https://faker.readthedocs.io/en/master/index.html
# https://factoryboy.readthedocs.io/en/stable/index.html
from factory import Faker
from factory.django import DjangoModelFactory
from faker import Factory

from ..models import Thing
from ..models import CreativeWork
from ..models import (
    Person,
    Organization,
    Place
                     )
from ..models import (
    Book,
    BookEdition
                     )
from ..models import (
    BookTextEdition,
    BookAudioEdition,
    BookDigitalTextEdition
                     )
from ..models import (
    BookEditionEbook,
    BookEditionKindle,
    BookEditionAudible,
    BookEditionHardcover,
    BookEditionPaperback,
    BookEditionAudioCD,
    BookEditionMassMarketPaperback,
    BookEditionAudiobook
                     )
from ..models import (
    Contributor,
    ContributorAuthor,
    ContributorNarrator,
    ContributorTranslator
                     )

faker = Factory.create()


class UserFactory(DjangoModelFactory):
    class Meta:
        model = User
    username = faker.profile()["username"]
    first_name = Faker('first_name')
    last_name = Faker('last_name')
    email = faker.email()  # Just to test a different method


class SuperUserFactory(DjangoModelFactory):
    class Meta:
        model = User
    username = faker.profile()["username"]
    first_name = Faker('first_name')
    last_name = Faker('last_name')
    email = faker.email()  # Just to test a different method

    is_superuser = True
    is_staff = True
    is_active = True


class PersonFactory(DjangoModelFactory):
    name = Faker('name')
    disambiguation = Faker('text')

    class Meta:
        model = Person
