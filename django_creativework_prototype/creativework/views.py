from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from rest_framework import permissions
from rest_framework.decorators import action
from rest_framework import mixins
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from .serializers import UserSerializer, GroupSerializer  # , BookThingSerializer, PersonThingSerializer
from .models import Thing
from . import serializers
from . import models


def _return_list_view(parent_view, serializer, queryset, request):
    queryset = parent_view.filter_queryset(queryset)
    page = parent_view.paginate_queryset(queryset)
    if page is not None:
        serializer_instance = serializer(page, many=True, context={'request': request})
        return parent_view.get_paginated_response(serializer_instance.data)

    serializer_instance = serializer(queryset, many=True, context={'request': request})
    return Response(serializer_instance.data)


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]

    @action(detail=True, methods=['get'])
    def groups(self, request, pk=None):
        groups = User.objects.get(id=pk).groups.all()
        return _return_list_view(self, queryset=groups, request=request, serializer=serializers.GroupSerializer)


'''class UserGroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.

    Only retrieval works. create, update, and destroy must be overloaded to do full CRUD on user group rel.
    """
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_queryset(self):
        user = User.objects.get(id=self.kwargs['users_pk'])
        return user.groups.all()'''


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated]


"""class BookViewSet(viewsets.ModelViewSet):
    queryset = models.Book.objects.all()
    serializer_class = serializers.BookSerializer
    permission_classes = [permissions.IsAuthenticated]"""


class BookViewSet(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                  mixins.RetrieveModelMixin,
                  mixins.UpdateModelMixin,
                  GenericViewSet):
    queryset = models.Book.objects.all()
    serializer_class = serializers.BookSerializer
    permission_classes = [permissions.IsAuthenticated]

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = serializers.BookListSerializer(page, many=True, context={'request': request})
            return self.get_paginated_response(serializer.data)

        serializer = serializers.BookListSerializer(queryset, many=True, context={'request': request})
        return Response(serializer.data)

    @action(detail=True, methods=['get'])
    def editions(self, request, pk=None):
        queryset = models.BookEdition.objects.filter(book_parent=pk)
        return _return_list_view(self, queryset=queryset, request=request,
                                 serializer=serializers.BookEditionPolymorphicSerializer)

    """def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = serializers.BookSerializer(instance=instance)
        return Response(serializer.data)
    
    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)"""


class PersonViewSet(viewsets.ModelViewSet):
    queryset = models.Person.objects.all()
    serializer_class = serializers.PersonSerializer
    permission_classes = [permissions.IsAuthenticated]

    def get_serializer(self, *args, **kwargs):
        # Use different serializers depending on method type
        kwargs['context'] = self.get_serializer_context()
        if getattr(self.request, "method") in ("PUT", "POST", "PATCH"):
            return serializers.PersonEditSerializer(*args, **kwargs)
        return self.serializer_class(*args, **kwargs)

    def update(self, request, *args, **kwargs):
        # Attempt to use different serializer on receiving data and return after update
        #   Ok-is. Form field in drf is not correct, but every thing else looks good. Form field is fixed after refresh.
        #   Good enough
        super().update(request, *args, **kwargs)
        instance = self.get_object()
        context = self.get_serializer_context()
        serializer = self.serializer_class(instance, context=context)
        return Response(serializer.data)


class BookEditionViewSet(viewsets.ModelViewSet):
    queryset = models.BookEdition.objects.all()
    serializer_class = serializers.BookEditionSerializer
    permission_classes = [permissions.IsAuthenticated]


class BookEditionPaperbackViewSet(viewsets.ModelViewSet):
    queryset = models.BookEditionPaperback.objects.all()
    serializer_class = serializers.BookEditionPaperbackSerializer
    permission_classes = [permissions.IsAuthenticated]


class PlaceViewSet(viewsets.ModelViewSet):
    queryset = models.Place.objects.all()
    serializer_class = serializers.PlaceSerializer
    permission_classes = [permissions.IsAuthenticated]

    @action(detail=True, methods=['get'])
    def birth_place_for(self, request, pk=None):
        queryset = models.Place.objects.get(id=pk).birth_place_for.all()
        return _return_list_view(self, queryset=queryset, request=request, serializer=serializers.PersonSerializer)


"""class ContributorViewSet(viewsets.ModelViewSet):
    queryset = models.Contributor.objects.all()
    serializer_class = serializers.ContributorSerializer
    permission_classes = [permissions.IsAuthenticated]"""


class OrganizationViewSet(viewsets.ModelViewSet):
    queryset = models.Organization.objects.all()
    serializer_class = serializers.OrganizationSerializer
    permission_classes = [permissions.IsAuthenticated]


class ThingViewSet(viewsets.ModelViewSet):
    queryset = Thing.objects.all()
    serializer_class = serializers.ThingListPolymorphicSerializer


class CreativeWorkViewSet(viewsets.ModelViewSet):
    queryset = models.CreativeWork.objects.all()
    serializer_class = serializers.CreativeWorkPolymorphicSerializer


########################################################################################################################
########################################################################################################################
########################################################################################################################


from django.views.generic import ListView, DetailView
from django.db.models import OuterRef, Subquery, Prefetch


class PersonListView(ListView):
    model = models.Person
    queryset = models.Person.objects.select_related("birth_place")


class BookListView(ListView):
    model = models.Book
    queryset = models.Book.objects \
        .prefetch_related(Prefetch("editions",
                                   # Warning: this is not logically corrent. It limits 2 editons for all books and not per book. No sure how to fix this in one query
                                   queryset=models.BookEdition.objects.filter(
                                       id__in=Subquery(
                                           models.BookEdition.objects.order_by("name").values_list('id', flat=True)[:2])
                                   ))
                          ) \
        .prefetch_related(Prefetch('contributors',  # This is logicaly correct. But it requires db flag
                                   queryset=models.Contributor.objects.filter(
                                       main_contributor=True).select_related('contributor')))

    # TODO: Should I add controll flags to Thing (or a controll table) that controlls queries like this?


class BookDetailView(DetailView):
    # template_name_field = "creativework/book_list.html"
    model = models.Book
    queryset = models.Book.objects.prefetch_related("editions") \
        .prefetch_related(Prefetch('contributors', queryset=models.Contributor.objects.select_related('contributor')))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class TvSeriesListView(ListView):
    model = models.TvSeries
    queryset = models.TvSeries.objects \
        .prefetch_related(
            Prefetch('has_parts',
                 queryset=models.Part.objects.select_related('is_part_of', 'has_part')
                     .prefetch_related(
                     Prefetch('has_part__has_parts',
                              queryset=models.Part.objects.select_related('is_part_of', 'has_part')
                              )
                 )
                     )
    )
