from django.apps import AppConfig


class CreativeworkConfig(AppConfig):
    name = 'creativework'
