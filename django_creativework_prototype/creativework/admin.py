from django.contrib import admin
from django import forms

from django.core.exceptions import PermissionDenied
from django.utils.translation import gettext_lazy as _
from django.contrib.contenttypes.models import ContentType

from polymorphic.admin import PolymorphicParentModelAdmin, PolymorphicChildModelAdmin, PolymorphicChildModelFilter

from . import models
from .models import Thing
from .models import CreativeWork
from .models import (
    Person,
    Organization,
    Place
                     )
from .models import (
    Book,
    BookEdition
                     )
from .models import (
    BookTextEdition,
    BookAudioEdition,
    BookDigitalTextEdition
                     )
from .models import (
    BookEditionEbook,
    BookEditionKindle,
    BookEditionAudible,
    BookEditionHardcover,
    BookEditionPaperback,
    BookEditionAudioCD,
    BookEditionMassMarketPaperback,
    BookEditionAudiobook
                     )
from .models import (
    Contributor,
    ContributorAuthor,
    ContributorNarrator,
    ContributorTranslator
                     )


class CustomPolymorphicChildModelFilter(admin.SimpleListFilter):

    title = _("Type")
    parameter_name = "polymorphic_ctype"

    def lookups(self, request, model_admin):
        temp =  model_admin.get_child_type_choices(request, "change")
        return temp

    def queryset(self, request, queryset):
        try:
            value = int(self.value())
        except TypeError:
            value = None
        if value:
            # ensure the content type is allowed
            for choice_value, _ in self.lookup_choices:
                if choice_value != value:
                    continue

                model = ContentType.objects.get_for_id(value).model_class()
                models = model.get_model_descendants_and_self()
                content_types = ContentType.objects.get_for_models(*models, for_concrete_models=False)
                valid_content_type_ids = set([x[0] for x in self.lookup_choices])

                choices = [ct.id for ct in content_types.values() if ct.id in valid_content_type_ids]

                return queryset.filter(polymorphic_ctype_id__in=choices)
            raise PermissionDenied(
                'Invalid ContentType "{0}". It must be registered as child model.'.format(
                    value
                )
            )
        return queryset


class LimitChoicesAdminMixin(admin.ModelAdmin):
    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        field = super().formfield_for_foreignkey(db_field, request, **kwargs)
        limit_choices_to_method = getattr(self.base_model, f"{db_field.name}_limit_choices_to_models", None)
        if callable(limit_choices_to_method):
            limits = [ContentType.objects.get_for_model(inst, for_concrete_model=False).id
                      for inst in limit_choices_to_method()]
            field.queryset = field.queryset.filter(polymorphic_ctype_id__in=limits)
        return field


class CustomPolymorphicChildModelAdmin(LimitChoicesAdminMixin, PolymorphicChildModelAdmin):
    pass


########################################################################################################################


@admin.register(Thing)
class ThingParentAdmin(PolymorphicParentModelAdmin):
    base_model = Thing  # Optional, explicitly set here.
    # child_models = (Person, Place, Organization, Book, BookEdition)
    list_filter = (CustomPolymorphicChildModelFilter,)  # This is optional.
    
    def get_child_models(self):
        return Thing.model_descendants[Thing]


class ChildOfThing(CustomPolymorphicChildModelAdmin):
    base_model = Thing  # Optional, explicitly set here.


@admin.register(Person)
class PersonAdmin(ChildOfThing):
    base_model = Person
    show_in_index = True





"""class PersonBornInline(admin.TabularInline):
    model = Person
    fk_name = "birth_place"
    extra = 0


class PersonDiedInline(admin.TabularInline):
    model = Person
    fk_name = "death_place"
    extra = 0


class PersonBuriedInline(admin.TabularInline):
    model = Person
    fk_name = "burial_place"
    extra = 0"""


@admin.register(Place)
class PlaceAdmin(ChildOfThing):
    base_model = Place
    show_in_index = True
    """inlines = [
        PersonBornInline,PersonDiedInline, PersonBuriedInline
    ]"""


@admin.register(Organization)
class OrganizationAdmin(ChildOfThing):
    base_model = Organization
    show_in_index = True


########################################################################################################################


@admin.register(CreativeWork)
class CreativeWorkParentAdmin(PolymorphicParentModelAdmin):
    base_model = CreativeWork  # Optional, explicitly set here.
    # child_models = (Book, BookEdition)
    list_filter = (PolymorphicChildModelFilter,)  # This is optional.
    # polymorphic_list = True

    def get_child_models(self):
        return CreativeWork.model_descendants[CreativeWork]


class CreativeWorkAdmin(ChildOfThing):
    base_model = CreativeWork
    show_in_index = False


@admin.register(Book)
class BookAdmin(CreativeWorkAdmin):
    base_model = Book
    show_in_index = True


@admin.register(models.Movie)
class MovieAdmin(CreativeWorkAdmin):
    base_model = models.Movie
    show_in_index = True


"""@admin.register(models.CreativeWorkSeries)
class CreativeWorkSeriesAdmin(CreativeWorkAdmin):
    base_model = models.CreativeWorkSeries
    show_in_index = False
"""

@admin.register(models.TvSeries)
class TvSeriesAdmin(CreativeWorkAdmin):
    base_model = models.TvSeries
    show_in_index = True


"""@admin.register(models.CreativeWorkSeason)
class CreativeWorkSeasonAdmin(CreativeWorkAdmin):
    base_model = models.CreativeWorkSeason
    show_in_index = False"""


@admin.register(models.TvSeason)
class TvSeasonAdmin(CreativeWorkAdmin):
    base_model = models.TvSeason
    show_in_index = True


"""@admin.register(models.CreativeWorkEpisode)
class CreativeWorkEpisodeAdmin(CreativeWorkAdmin):
    base_model = models.CreativeWorkEpisode
    show_in_index = False"""


class TvEpisodeAdminForm(forms.ModelForm):

    series_custom = forms.ModelChoiceField(queryset=models.TvSeries.objects.all(), required=False)
    series_order_custom = forms.FloatField(required=False)

    season_custom = forms.ModelChoiceField(queryset=models.TvSeason.objects.all(), required=False)
    season_order_custom = forms.FloatField(required=False)

    def __init__(self, *args, **kwargs):
        initial_arguments = kwargs.get('initial', {})

        instance = kwargs.get('instance')
        if instance:
            initial_arguments['series_custom'] = instance.series
            initial_arguments['series_order_custom'] = instance.series_order
            initial_arguments['season_custom'] = instance.season
            initial_arguments['season_order_custom'] = instance.season_order

        kwargs.update(initial=initial_arguments)

        super(TvEpisodeAdminForm, self).__init__(*args, **kwargs)

        """instance = kwargs.get('instance')
        if instance:
            self.fields['series'].initial = instance.series
            # self.fields['season'].initial = instance.season"""
        instance2 = instance

    def save(self, commit=True):
        series_custom_field = self.cleaned_data.get('series_custom', None)
        series_order_custom_field = self.cleaned_data.get('series_order_custom', None)
        seasons_custom_field = self.cleaned_data.get('season_custom', None)
        season_order_custom_field = self.cleaned_data.get('season_order_custom', None)
        self.instance.series = series_custom_field
        self.instance.series_order = series_order_custom_field
        self.instance.season = seasons_custom_field
        self.instance.season_order = season_order_custom_field
        return super(TvEpisodeAdminForm, self).save(commit=commit)

    class Meta:
        model = models.TvEpisode
        fields = "__all__"

@admin.register(models.TvEpisode)
class TvEpisodeAdmin(CreativeWorkAdmin):
    base_model = models.TvEpisode
    show_in_index = True
    form = TvEpisodeAdminForm


########################################################################################################################

@admin.register(models.Part)
class PartParentAdmin(PolymorphicParentModelAdmin):
    base_model = models.Part  # Optional, explicitly set here.
    # child_models = (Person, Place, Organization, Book, BookEdition)
    list_filter = (CustomPolymorphicChildModelFilter,)  # This is optional.

    def get_child_models(self):
        return models.Part.get_model_descendants()


class ChildOfPart(CustomPolymorphicChildModelAdmin):
    base_model = models.Part  # Optional, explicitly set here.


"""@admin.register(models.SeriesPart)
class SeriesPartAdmin(ChildOfPart):
    base_model = models.SeriesPart
    show_in_index = True"""


@admin.register(models.SeasonPart)
class SeasonPartAdmin(ChildOfPart):
    base_model = models.SeasonPart
    show_in_index = True


@admin.register(models.EpisodePart)
class EpisodePartAdmin(ChildOfPart):
    base_model = models.EpisodePart
    show_in_index = True


########################################################################################################################


@admin.register(BookEdition)
class BookEditionParentAdmin(PolymorphicParentModelAdmin):
    base_model = BookEdition  # Optional, explicitly set here.
    """child_models = (BookEditionAudible, BookEditionAudioCD, BookEditionAudiobook,
                    BookEditionEbook, BookEditionKindle,
                    BookEditionHardcover, BookEditionPaperback, BookEditionMassMarketPaperback)"""
    list_filter = (PolymorphicChildModelFilter,)  # This is optional.
    polymorphic_list = False

    def get_child_models(self):
        return BookEdition.model_descendants[BookEdition]


class BookEditionAdmin(CreativeWorkAdmin):
    base_model = BookEdition
    show_in_index = False


@admin.register(BookAudioEdition)
class BookAudioEditionParentAdmin(PolymorphicParentModelAdmin):
    base_model = BookAudioEdition  # Optional, explicitly set here.
    # child_models = (BookEditionAudible, BookEditionAudioCD, BookEditionAudiobook)
    list_filter = (PolymorphicChildModelFilter,)  # This is optional.

    def get_child_models(self):
        return BookAudioEdition.model_descendants[BookAudioEdition]


class BookAudioEditionAdmin(BookEditionAdmin):
    base_model = BookAudioEdition
    show_in_index = False


@admin.register(BookDigitalTextEdition)
class BookDigitalTextEditionParentAdmin(PolymorphicParentModelAdmin):
    base_model = BookDigitalTextEdition  # Optional, explicitly set here.
    # child_models = (BookEditionEbook, BookEditionKindle)
    list_filter = (PolymorphicChildModelFilter,)  # This is optional.

    def get_child_models(self):
        return BookDigitalTextEdition.model_descendants[BookDigitalTextEdition]


class BookDigitalTextEditionAdmin(BookEditionAdmin):
    base_model = BookDigitalTextEdition
    show_in_index = False


@admin.register(BookTextEdition)
class BookTextEditionParentAdmin(PolymorphicParentModelAdmin):
    base_model = BookTextEdition  # Optional, explicitly set here.
    # child_models = (BookEditionHardcover, BookEditionPaperback, BookEditionMassMarketPaperback)
    list_filter = (PolymorphicChildModelFilter,)  # This is optional.

    def get_child_models(self):
        return BookTextEdition.model_descendants[BookTextEdition]


class BookTextEditionAdmin(BookEditionAdmin):
    base_model = BookTextEdition
    show_in_index = False


@admin.register(BookEditionEbook)
class BookEditionEbookAdmin(BookDigitalTextEditionAdmin):
    base_model = BookEditionEbook
    show_in_index = True


@admin.register(BookEditionKindle)
class BookEditionKindleAdmin(BookDigitalTextEditionAdmin):
    base_model = BookEditionKindle
    show_in_index = True


@admin.register(BookEditionAudible)
class BookEditionAudibleAdmin(BookAudioEditionAdmin):
    base_model = BookEditionAudible
    show_in_index = True


@admin.register(BookEditionHardcover)
class BookEditionHardcoverAdmin(BookTextEditionAdmin):
    base_model = BookEditionHardcover
    show_in_index = True


@admin.register(BookEditionPaperback)
class BookEditionPaperbackAdmin(BookTextEditionAdmin):
    base_model = BookEditionPaperback
    show_in_index = True


@admin.register(BookEditionAudioCD)
class BookEditionAudioCDAdmin(BookAudioEditionAdmin):
    base_model = BookEditionAudioCD
    show_in_index = True


@admin.register(BookEditionMassMarketPaperback)
class BookEditionMassMarketPaperbackAdmin(BookTextEditionAdmin):
    base_model = BookEditionMassMarketPaperback
    show_in_index = True


@admin.register(BookEditionAudiobook)
class BookEditionAudiobookAdmin(BookAudioEditionAdmin):
    base_model = BookEditionAudiobook
    show_in_index = True


########################################################################################################################


@admin.register(Contributor)
class CreativeWorkParentAdmin(PolymorphicParentModelAdmin):
    base_model = Contributor  # Optional, explicitly set here.
    # child_models = (ContributorAuthor, ContributorTranslator, ContributorNarrator)
    list_filter = (PolymorphicChildModelFilter,)  # This is optional.

    def get_child_models(self):
        return Contributor.model_descendants[Contributor]


class ChildOfContributor(CustomPolymorphicChildModelAdmin):
    base_model = Contributor  # Optional, explicitly set here.


class ContributorAdmin(ChildOfContributor):
    base_model = ContributorAuthor
    show_in_index = False

@admin.register(ContributorAuthor)
class ContributorAuthorAdmin(ContributorAdmin):
    base_model = ContributorAuthor
    show_in_index = True


@admin.register(ContributorTranslator)
class ContributorTranslatorAdmin(ContributorAdmin):
    base_model = ContributorTranslator
    show_in_index = True


@admin.register(ContributorNarrator)
class ContributorNarratorAdmin(ContributorAdmin):
    base_model = ContributorNarrator
    show_in_index = True


@admin.register(models.ContributorActor)
class ContributorActorAdmin(ContributorAdmin):
    base_model = models.ContributorActor
    show_in_index = True


@admin.register(models.ContributorDirector)
class ContributorDirectorAdmin(ContributorAdmin):
    base_model = models.ContributorDirector
    show_in_index = True
