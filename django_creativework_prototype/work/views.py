from django.shortcuts import render
from django.views.generic import ListView, DetailView
from django.db.models import OuterRef, Subquery, Prefetch

from . import models


########################################################################################################################
########################################################################################################################
########################################################################################################################


class TvShowListView(ListView):
    model = models.TvShow
    queryset = models.TvShow.objects \
        .select_related("creativework") \
        .prefetch_related(Prefetch("tvseason_set",
                                   queryset=models.TvSeason.objects
                                   .select_related("creativework")
                                   .prefetch_related(Prefetch("tvepisode_set",
                                                              queryset=models.TvEpisode.objects
                                                              .select_related("creativework")))
                                   ),
                          )