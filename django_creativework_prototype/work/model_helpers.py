from django.db import models
from django.core.exceptions import ValidationError


class LimitChoicesMixin:
    """def clean(self):
        # TODO: Currently not working
        field_names = [f.name for f in self._meta.get_fields()]
        for field_name in field_names:
            value = getattr(self, field_name, None)
            if value is None:
                continue
            limit_choices_to_method = getattr(self, f"{field_name}_limit_choices_to_models", None)
            if callable(limit_choices_to_method):
                valid_choice_models = limit_choices_to_method()
                if type(value) not in valid_choice_models:
                    raise ValidationError(_(f'Field {field_name} with type {type(value)} is invalid. Must be of type {valid_choice_models}'))"""


class CustomModelHelpers:

    def add_helper(self, name, value, valid_types=None, null=True):
        if not null and value is null:
            raise ValueError(f"value can not be None")
        if valid_types and not any(isinstance(value, valid_type) for valid_type in valid_types):
            raise ValueError(f"{value} must be of type(s) {valid_types}")

        field_name = f"_cached_{name}"
        existing_value = getattr(self, field_name, None)

        try:
            if existing_value.id == value.id:
                return
        except Exception:
            if existing_value == value:
                return
        setattr(self, field_name, value)
        setattr(self, f"{field_name}_is_set", value)

    def get_helper_from_func(self, name, func):
        def process(*args, **kwargs):
            field_name = f"_cached_{name}"
            if hasattr(self, field_name):
                return getattr(self, field_name)
            val = func(*args, **kwargs)
            if val:
                setattr(self, field_name, val)
            return val
        return process


class CustomPolymorphicModelMixin(CustomModelHelpers, models.Model):
    custom_polymorphic_model_marker = True
    model_children = dict()
    model_descendants = dict()

    """def load_child(self, child):
        if type(child) not in self.get_model_descendants_and_self():
            raise ValidationError(f"{child} is not in valid list {self.get_model_descendants_and_self()}")
        child.polymorphic_ctype_id = self.polymorphic_ctype_id"""

    @classmethod
    def get_model_children(cls):
        return cls.model_children.get(cls, [])

    @classmethod
    def get_model_children_and_self(cls):
        model_list = cls.get_model_children()
        model_list.append(cls)
        return model_list

    @classmethod
    def get_model_descendants(cls):
        return cls.model_descendants.get(cls, [])

    @classmethod
    def get_model_descendants_and_self(cls):
        model_list = cls.get_model_descendants()
        model_list.append(cls)
        return model_list

    def __init_subclass__(cls, **kwargs):
        def add_model_descendants(obj):
            if not CustomPolymorphicModelMixin.object_is_polymorphic_model(obj):
                return
            for inner_base in obj.__bases__:
                add_model_descendants(inner_base)
            add_to_dict_in_model(obj, "model_descendants")

        def add_model_children(obj):
            if not CustomPolymorphicModelMixin.object_is_polymorphic_model(obj):
                return
            add_to_dict_in_model(obj, "model_children")

        def add_to_dict_in_model(obj, dict_name):
            if hasattr(obj, dict_name):
                dict_inst = getattr(obj, dict_name)
                if obj not in dict_inst:
                    dict_inst[obj] = list()
                dict_inst[obj].append(cls)

        for base in cls.__bases__:
            add_model_children(base)
            add_model_descendants(base)

        super().__init_subclass__(**kwargs)

    class Meta:
        abstract = True
        base_manager_name = "objects"

    @staticmethod
    def object_is_polymorphic_model(cls):
        if not hasattr(cls, "custom_polymorphic_model_marker"):
            return False
        if not getattr(cls, "custom_polymorphic_model_marker"):
            return False
        if str(cls) == str(CustomPolymorphicModelMixin):  # Fix it
            return False
        return True