from django.db import models
from django.conf import settings
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ValidationError
from django.db.models import Q, F

from polymorphic.models import PolymorphicModel, PolymorphicManager
import langcodes

from .model_helpers import CustomPolymorphicModelMixin


########################################################################################################################
#
#       Base
#
#############


class Thing(CustomPolymorphicModelMixin, PolymorphicModel):
    id = models.BigAutoField(primary_key=True)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    creator = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, null=True, blank=True)

    name = models.TextField()
    disambiguation = models.TextField(blank=True, null=True)

    def __str__(self):
        return str(self.name)

    def __unicode__(self):
        return self.__str__()


#############


class ExternalSource(Thing):
    pass


class ExternalReference(models.Model):
    thing = models.ForeignKey(Thing, on_delete=models.CASCADE,
                              related_name="reference_set",
                              related_query_name="reference"
                              )
    source = models.ForeignKey(ExternalSource, on_delete=models.CASCADE,
                               related_name="source_reference_set",
                               related_query_name="source_reference")
    reference = models.TextField()

    class Meta:
        unique_together = ('thing', 'source', 'reference')


########################################################################################################################
#
#       CreativeWork
#
#############


def validate_language(value):
    if value is None:
        return
    langcodes.Language.get(value).is_valid()
    if not langcodes.Language.get(value).is_valid():
        raise ValidationError(
            _('%(value)s is not a valid language'),
            params={'value': value},
        )


class CreativeWork(models.Model):
    class CreativeWorkStatus(models.IntegerChoices):
        INCOMPLETE = 1, _('Incomplete')
        DRAFT = 2, _('Draft')
        PUBLISHED = 3, _('Published')
        OBSOLETE = 4, _('Obsolete')

    thing = models.OneToOneField(Thing, on_delete=models.CASCADE, primary_key=True)

    creative_work_status = models.IntegerField(
        choices=CreativeWorkStatus.choices,
        default=None,
        blank=True, null=True
    )

    publish_date = models.DateField(blank=True, null=True)
    # original_title = models.TextField(blank=True, null=True)

    language = models.TextField(null=True, blank=True, validators=[validate_language])

    def save(self, **kwargs):
        if self.language:
            lang = langcodes.Language.get(self.language)
            self.language = langcodes.standardize_tag(str(lang))
        instance = super().save(**kwargs)
        return instance


########################################################################################################################
#
#       TV
#
#############


class TvShow(Thing):

    @property
    def seasons(self):
        return self.tvseason_set.all()

    @property
    def episodes(self):
        for season in self.seasons:
            for episode in season.episodes:
                yield episode


class TvSeason(Thing):
    show = models.ForeignKey(TvShow, on_delete=models.PROTECT)

    @property
    def episodes(self):
        return self.tvepisode_set.all()


class TvEpisode(Thing):

    season = models.ForeignKey(TvSeason, on_delete=models.PROTECT)

    @property
    def show(self):
        return self.season.show


########################################################################################################################
#
#       Movie
#
#############


class Movie(Thing):
    pass


########################################################################################################################
#
#       Book
#
#############


class Book(Thing):
    pass


class BookEdition(Thing):
    # book = models.ForeignKey(Book, blank=True, null=True)
    book_parent = models.ForeignKey(Book, null=True, blank=True,
                                    on_delete=models.SET_NULL,
                                    related_name="edition_set",
                                    related_query_name="edition")

    abridged = models.BooleanField(blank=True, null=True)

    duration = models.IntegerField(blank=True, null=True)
    pages = models.IntegerField(blank=True, null=True)

    # TODO: Should some of thise (asin) be moved into the ExternalReference table
    # isbn = models.CharField(max_length=10, blank=True, null=True)
    # isbn13 = models.CharField(max_length=13, blank=True, null=True)
    # asin = models.CharField(max_length=10, blank=True, null=True)


########################################################################################################################
#
#       Div Things
#
#############


"""class Place(Thing):
    pass"""


"""class Company(Thing):
    pass


class Person(Thing):
    pass"""


########################################################################################################################
#
#       Contributor
#
#############


"""class ContributedWork(models.Model):
    pass


class Contributor(models.Model):
    pass


class Contribution(CustomPolymorphicModelMixin, PolymorphicModel):
    work = models.ForeignKey(ContributedWork, null=False, blank=False,
                             on_delete=models.CASCADE,
                             related_name="contributor_set",
                             related_query_name="contributor")

    contributor = models.ForeignKey(Contributor, null=False, blank=False,
                                    on_delete=models.CASCADE,
                                    related_name="contribution_set",
                                    related_query_name="contribution")"""


"""class Contributor(CustomPolymorphicModelMixin, PolymorphicModel):
    work = models.ForeignKey(Thing, null=False, blank=True,
                             on_delete=models.PROTECT,
                             related_name="contributor_set",
                             related_query_name="contributor")
    
    contributor = models.ForeignKey(Thing, null=False, blank=True, editable=False,
                                    on_delete=models.PROTECT,
                                    related_name="contribution_set",
                                    related_query_name="contribution")

    contributor_person = models.ForeignKey(Person, null=True, blank=True,
                                    on_delete=models.PROTECT,
                                    related_name="contribution_person_set",
                                    related_query_name="contribution_person")

    contributor_company = models.ForeignKey(Company, null=True, blank=True,
                                    on_delete=models.PROTECT,
                                    related_name="contribution_company_set",
                                    related_query_name="contribution_company")

    class Meta:
        constraints = [
            models.CheckConstraint(
                check=Q(contributor_id=F('contributor_person_id')) | Q(contributor_id=F('contributor_company_id')),
                name='contributor_values_must_match')
        ]

    def save(self, **kwargs):
        if self.contributor_person:
            self.contributor_id = self.contributor_person_id
        elif self.contributor_company:
            self.contributor_id = self.contributor_company_id
            
        super().save(**kwargs)"""