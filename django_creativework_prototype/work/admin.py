from django.contrib import admin

from polymorphic.admin import PolymorphicParentModelAdmin, PolymorphicChildModelAdmin, PolymorphicChildModelFilter

from . import models


class CreativeWorkInline(admin.StackedInline):
    model = models.CreativeWork


class ExternalReferenceInline(admin.TabularInline):
    model = models.ExternalReference
    fk_name = "thing"
    extra = 0


@admin.register(models.Thing)
class ThingParentAdmin(PolymorphicParentModelAdmin):
    base_model = models.Thing  # Optional, explicitly set here.
    # child_models = (Person, Place, Organization, Book, BookEdition)
    list_filter = (PolymorphicChildModelFilter,)  # This is optional.

    def get_child_models(self):
        return models.Thing.get_model_descendants()


class ChildOfThing(PolymorphicChildModelAdmin):
    base_model = models.Thing  # Optional, explicitly set here.
    inlines = PolymorphicChildModelAdmin.inlines + [ExternalReferenceInline, ]


@admin.register(models.ExternalSource)
class ExternalSourceAdmin(ChildOfThing):
    base_model = models.ExternalSource
    show_in_index = True


class ChildOfThingThatIsCreativeWork(ChildOfThing):
    inlines = ChildOfThing.inlines + [CreativeWorkInline, ]


@admin.register(models.TvShow)
class TvShowAdmin(ChildOfThingThatIsCreativeWork):
    base_model = models.TvShow
    show_in_index = True


@admin.register(models.TvSeason)
class TvSeasonAdmin(ChildOfThingThatIsCreativeWork):
    base_model = models.TvSeason
    show_in_index = True


@admin.register(models.TvEpisode)
class TvEpisodeAdmin(ChildOfThingThatIsCreativeWork):
    base_model = models.TvEpisode
    show_in_index = True
    readonly_fields = ('show',)

    def show(self, instance):
        return instance.show


@admin.register(models.Movie)
class MovieAdmin(ChildOfThingThatIsCreativeWork):
    base_model = models.Movie
    show_in_index = True


@admin.register(models.Book)
class BookAdmin(ChildOfThingThatIsCreativeWork):
    base_model = models.Book
    show_in_index = True


@admin.register(models.BookEdition)
class BookEditionAdmin(ChildOfThingThatIsCreativeWork):
    base_model = models.BookEdition
    show_in_index = True