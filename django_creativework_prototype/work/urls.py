from django.urls import include, path

from . import views

urlpatterns = [
    path('shows/', views.TvShowListView.as_view(), name="view-shows-list"),
]
