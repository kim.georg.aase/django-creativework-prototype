from django.apps import AppConfig


class WorkyConfig(AppConfig):
    name = 'worky'
