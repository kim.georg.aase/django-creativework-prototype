from django.contrib import admin
from django import forms
from django.contrib.contenttypes.admin import GenericTabularInline

from . import models


class ContributorAdminInLine(GenericTabularInline):
    model = models.Contribution
    ct_field = "work_content_type"
    ct_fk_field = "work_id"
    extra = 0
    exclude = ["contributor_content_type", "contribution_type"]


class ContributorActorAdminInLine(GenericTabularInline):
    model = models.ContributionActor
    ct_field = "work_content_type"
    ct_fk_field = "work_id"
    extra = 0
    exclude = ["contributor_content_type", "contribution_type"]


@admin.register(models.ExternalSource)
class ExternalSourceAdmin(admin.ModelAdmin):
    base_model = models.ExternalSource
    exclude = ["entity", ]


class ExternalReferenceAdminInLine(GenericTabularInline):
    model = models.ExternalReference
    ct_field = "target_content_type"
    ct_fk_field = "target_id"
    extra = 0


@admin.register(models.Person)
class PersonAdmin(admin.ModelAdmin):
    base_model = models.Person
    exclude = ["entity", "contribution_relationship"]


@admin.register(models.Contribution)
class ContributionAdmin(admin.ModelAdmin):
    exclude = ["entity", "contributor_relationship"]
    base_model = models.Contribution


@admin.register(models.Book)
class BookAdmin(admin.ModelAdmin):
    base_model = models.Book
    exclude = ["entity", "contributor_relationship"]
    inlines = [ContributorAdminInLine, ContributorActorAdminInLine, ExternalReferenceAdminInLine]


@admin.register(models.Paperback)
class PaperbackAdmin(admin.ModelAdmin):
    base_model = models.Paperback
    exclude = ["entity", "contributor_relationship", "format", "duration"]
    inlines = [ContributorAdminInLine, ContributorActorAdminInLine, ExternalReferenceAdminInLine]


@admin.register(models.TvShow)
class TvShowAdmin(admin.ModelAdmin):
    base_model = models.TvShow
    exclude = ["entity", "contributor_relationship"]
    # filter_horizontal = ('references',)
    inlines = [ContributorAdminInLine, ContributorActorAdminInLine, ExternalReferenceAdminInLine]

@admin.register(models.TvSeason)
class TvSeasonAdmin(admin.ModelAdmin):
    base_model = models.TvSeason
    exclude = ["entity", "contributor_relationship"]
    inlines = [ContributorAdminInLine, ContributorActorAdminInLine, ExternalReferenceAdminInLine]


@admin.register(models.TvEpisode)
class TvEpisodeAdmin(admin.ModelAdmin):
    base_model = models.TvEpisode
    exclude = ["entity", "contributor_relationship"]
