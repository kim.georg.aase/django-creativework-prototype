from django.db import models
from django.conf import settings
from django.urls import reverse
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ValidationError
from django.db.models import Q, F
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation

import langcodes
# from typedmodels.models import TypedModel
# from polymorphic.models import PolymorphicModel, PolymorphicManager

# from .model_helpers import CustomPolymorphicModelMixin


########################################################################################################################
#
#       Thing
#
#############


class OrderMixin(models.Model):
    order = models.FloatField(null=True, blank=True, default=None)  # Specified order (lowest first, highest last)
    weight = models.FloatField(null=True, blank=True, default=None)  # Calculated weight (highest first, lowest last)

    class Meta:
        abstract = True


class RelationMixin(models.Model):
    id = models.BigIntegerField(primary_key=True)
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, editable=False)

    class Meta:
        abstract = True
        unique_together = ('id', 'content_type')

    def __str__(self):
        return f"{self.content_type.model} - {str(getattr(self, self.content_type.model).name)}"

    def __unicode__(self):
        return self.__str__()


class EntityRelation(RelationMixin, models.Model):
    id = models.BigAutoField(primary_key=True)


class Entity(models.Model):
    # id = models.BigAutoField(primary_key=True)
    entity = models.OneToOneField(EntityRelation, primary_key=True, blank=True, null=False, on_delete=models.CASCADE)

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    creator = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.PROTECT, null=True, blank=True)

    name = models.TextField()
    disambiguation = models.TextField(blank=True, null=True)

    # referable = models.OneToOneField("Referable", null=False, blank=True)

    # references = models.ManyToManyField("ExternalReference", blank=True)

    references = GenericRelation("ExternalReference", object_id_field="target_id", content_type_field="target_content_type")

    def __str__(self):
        return str(self.name)

    def __unicode__(self):
        return self.__str__()

    def save(self, **kwargs):
        if not self.entity_id:
            self.entity = EntityRelation.objects.create(
                content_type=ContentType.objects.get_for_model(self._meta.model))
        """if not self.referable:
            self.referable = Referable.objects.create()"""

        super().save(**kwargs)

    class Meta:
        abstract = True


########################################################################################################################
#
#       Contributor
#
#############


class ContributorRelation(RelationMixin, models.Model):
    pass


class ContributorMixin(models.Model):
    contribution_relationship = models.OneToOneField(ContributorRelation, null=True, blank=True, on_delete=models.CASCADE, editable=False)

    class Meta:
        abstract = True

    def save(self, **kwargs):
        if not self.contribution_relationship_id:
            self.contribution_relationship = ContributorRelation.objects.create(
                id=self.pk, content_type=ContentType.objects.get_for_model(self._meta.model))
        super().save(**kwargs)


class ContributableRelation(RelationMixin, models.Model):
    pass


class ContributableMixin(models.Model):
    contributor_relationship = models.OneToOneField(ContributableRelation, null=True, blank=True, on_delete=models.CASCADE, editable=False)

    class Meta:
        abstract = True

    def save(self, **kwargs):
        if not self.contributor_relationship_id:
            self.contributor_relationship = ContributableRelation.objects.create(
                id=self.pk, content_type=ContentType.objects.get_for_model(self._meta.model))
        super().save(**kwargs)


class ContributionType(models.IntegerChoices):
    CONTRIBUTOR = 0, _('Contributor')
    # AUTHOR = 1, _('Author')
    # TRANSLATOR = 2, _('Translator')
    # NARRATOR = 3, _('Narrator')
    # ILLUSTRATOR = 4, _('Illustrator')
    # EDITOR = 5, _('Editor')
    # COPYRIGHT_HOLDER = 6, _('Copyright Holder')
    # CREATOR = 7, _('Creator')
    # FUNDER = 8, _('Funder')
    # Maintainer = 9, _('Maintainer')
    # PRODUCER = 10, _('Producer')
    # PROVIDER = 11, _('Provider')
    # PUBLISHER = 12, _('Publisher')
    # SPONSOR = 13, _('Sponsor')
    # CHARACTER = 14, _('character')
    # ACCOUNTABLE_PERSON = 15, _('Accountable Person')
    ACTOR = 16, _('Actor')
    # MUSIC = 17, _('Music')
    # DIRECTOR = 18, _('Director')


class Contribution(OrderMixin, models.Model):
    work = models.ForeignKey(ContributableRelation, null=False, blank=False,
                             on_delete=models.CASCADE,
                             related_name="contributor_set",
                             related_query_name="contributor")
    work_content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE,
                                          related_name="contributor_content_type")
    work_content_object = GenericForeignKey('work_content_type', 'work_id')

    contributor = models.ForeignKey(ContributorRelation, null=False, blank=False,
                                    on_delete=models.CASCADE,
                                    related_name="contribution_set",
                                    related_query_name="contribution")
    contributor_content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE,
                                                 related_name="contribution_content_type")
    contributor_content_object = GenericForeignKey('contributor_content_type', 'contributor_id')

    is_main_contribution = models.BooleanField(blank=True, null=True)
    contribution_type = models.IntegerField(choices=ContributionType.choices, blank=True, null=True, default=None)
    contribution_type_disambiguation = models.TextField(blank=True, null=True)

    class Meta:
        unique_together = ('work', 'contributor')

    def __str__(self):
        return f"{self.work} | {self.contributor}"

    def __unicode__(self):
        return self.__str__()

    def _work_is_valid_type(self):
        """
        Used for more specific code level constraint on relation (Not going to be enforced on db level)
        """
        return True

    def _ensure_work_is_valid_type(self):
        if not self._work_is_valid_type():
            raise ValidationError("work is not a valid type")

    def _set_contribution_type(self):
        self.contribution_type = ContributionType.CONTRIBUTOR

    def save(self, **kwargs):
        self._ensure_work_is_valid_type()  # work was not set during clean, why? So called in save
        if self.contribution_type is None:
            self.contribution_type = ContributionType.CONTRIBUTOR
        self.work_content_type = self.work.content_type
        self.contributor_content_type = self.contributor.content_type
        super().save(**kwargs)


#############


class ContributionActorManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(contribution_type=ContributionType.ACTOR)


class CanHaveActorContributionMixin:
    _can_have_actor_contribution = True


class ContributionActor(Contribution):
    """
    Actor is special and need some extra later. It need to be connected to Character. Not done for now.
        Currently not sure if just gona add a third relation that is null in most other types, or ig actor is gona be split in new table
    """
    objects = ContributionActorManager()

    class Meta:
        proxy = True
        verbose_name = "actor"

    def _work_is_valid_type(self):
        return getattr(self.work_content_object, "_can_have_actor_contribution", False)

    def _set_contribution_type(self):
        self.contribution_type = Contribution.ACTOR



########################################################################################################################
#
#       CreativeWork
#
#############


def validate_language(value):
    if value is None:
        return
    langcodes.Language.get(value).is_valid()
    if not langcodes.Language.get(value).is_valid():
        raise ValidationError(
            _('%(value)s is not a valid language'),
            params={'value': value},
        )


class CreativeWork(Entity, ContributableMixin):
    class CreativeWorkStatus(models.IntegerChoices):
        INCOMPLETE = 1, _('Incomplete')
        DRAFT = 2, _('Draft')
        PUBLISHED = 3, _('Published')
        OBSOLETE = 4, _('Obsolete')

    # creative_work_type = models.OneToOneField(CreativeWorkType, null=False, blank=True, on_delete=models.CASCADE)

    creative_work_status = models.IntegerField(
        choices=CreativeWorkStatus.choices,
        default=None,
        blank=True, null=True
    )

    publish_date = models.DateField(blank=True, null=True)
    # original_title = models.TextField(blank=True, null=True)

    language = models.TextField(null=True, blank=True, validators=[validate_language])

    class Meta:
        abstract = True

    def save(self, **kwargs):
        """if not self.creative_work_type_id:
            self.creative_work_type = CreativeWorkType.objects.create(
                id=self.pk, content_type=ContentType.objects.get_for_model(self._meta.model))"""
        if self.language:
            lang = langcodes.Language.get(self.language)
            self.language = langcodes.standardize_tag(str(lang))
        instance = super().save(**kwargs)
        return instance


########################################################################################################################
#
#       Div Things
#
#############


"""class Place(Thing):
    pass"""


class Party(Entity, ContributorMixin):
    class Meta:
        abstract = True


class Company(Party, models.Model):
    pass


class Person(Party, models.Model):
    pass


########################################################################################################################
#
#       TV
#
#############


class TvShow(CreativeWork, CanHaveActorContributionMixin, models.Model):
    # TODO: Rename to Show?
    @property
    def seasons(self):
        return self.season_set.all()

    @property
    def episodes(self):
        for season in self.seasons:
            for episode in season.episodes:
                yield episode

    def save(self, **kwargs):
        super().save(**kwargs)


class TvSeason(CreativeWork, models.Model):
    # TODO: Rename to Season?
    show = models.ForeignKey("TvShow", on_delete=models.PROTECT,
                             null=False, blank=False,
                             related_name="season_set",
                             related_query_name="season"
                             )

    @property
    def episodes(self):
        return self.episode_set.all()

    def save(self, **kwargs):
        super().save(**kwargs)


class TvEpisode(CreativeWork, models.Model):
    # TODO: Rename to Episode?
    season = models.ForeignKey("TvSeason", on_delete=models.PROTECT,
                               null=False, blank=False,
                               related_name="episode_set",
                               related_query_name="episode"
                               )

    @property
    def show(self):
        return self.season.show

    def save(self, **kwargs):
        super().save(**kwargs)


########################################################################################################################
#
#       TV
#
#############


class Book(CreativeWork, models.Model):
    pass


class BookFormat(models.IntegerChoices):
    # UNKNOWN = 0, _('Unknown')
    EBOOK = 1, _('Ebook')
    PAPERBACK = 2, _('Paperback')
    AUDIBLE = 3, _('Audible')
    KINDLE = 4, _('Kindle')
    HARDCOVER = 5, _('Hardcover')
    AUDIOBOOK = 6, _('Audiobook')
    AUDIO_CD = 7, _('Audio CD')
    MASS_MARKET_PAPERBACK = 8, _('Mass Market Paperback')


class Edition(CreativeWork):
    # TODO: Edition or BookEdition as name?
    book = models.ForeignKey(Book, null=True, blank=True,
                                    on_delete=models.SET_NULL,
                                    related_name="editions")

    format = models.IntegerField(choices=BookFormat.choices, blank=True, null=True, default=None)

    abridged = models.BooleanField(blank=True, null=True)

    duration = models.IntegerField(blank=True, null=True)
    pages = models.IntegerField(blank=True, null=True)

    # TODO: Should some of thise (asin) be moved into the ExternalReference table
    isbn = models.CharField(max_length=10, blank=True, null=True)
    isbn13 = models.CharField(max_length=13, blank=True, null=True)
    # asin = models.CharField(max_length=10, blank=True, null=True)

    def is_audiobook(self):
        return self.format in (BookFormat.AUDIOBOOK, BookFormat.AUDIO_CD, BookFormat.AUDIBLE)

    def is_digital(self):
        return self.format in (BookFormat.EBOOK, BookFormat.KINDLE)

    def is_physical_text(self):
        # TODO: Not happy with name
        return self.format in (BookFormat.PAPERBACK, BookFormat.HARDCOVER, BookFormat.MASS_MARKET_PAPERBACK)

    def save(self, **kwargs):
        if self.format is not None and self.format not in BookFormat.values:
            self.format = None

        super().save(**kwargs)


class PaperbackManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(format=BookFormat.PAPERBACK)


class Paperback(Edition):
    objects = PaperbackManager()

    class Meta:
        proxy = True
        verbose_name = "paperback"

    def save(self, **kwargs):
        self.format = BookFormat.PAPERBACK


########################################################################################################################
#
#       ExternalReference
#
#############


class ExternalSource(Entity, models.Model):
    pass


class ExternalReference(models.Model):
    id = models.BigAutoField(primary_key=True)

    source = models.ForeignKey(ExternalSource, on_delete=models.CASCADE,
                               related_name="source_reference_set",
                               related_query_name="source_reference")

    target = models.ForeignKey(EntityRelation, on_delete=models.CASCADE,
                               related_name="reference_set",
                               related_query_name="reference"
                               )
    target_content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE,
                                                 related_name="reference_target_content_type")
    target_content_object = GenericForeignKey('target_content_type', 'target_id')

    value = models.TextField()

    class Meta:
        unique_together = ('target', 'source', 'value')

    def save(self, **kwargs):
        self.target_content_type = self.target.content_type
        super().save(**kwargs)

    def __str__(self):
        # Note: This and other str rep are not going to be like this
        return f"{self.source} | {self.target} | {self.value}"

    def __unicode__(self):
        return self.__str__()