from django.urls import include, path
from rest_framework import routers

from . import views


router = routers.DefaultRouter()
router.register(r'internals/users', views.UserViewSet)
router.register(r'internals/groups', views.GroupViewSet)

#router.register(r'books', views.)
# router.register(r'books/editions', views.BookEditionViewSet)
#router.register(r'books/editions/paperbacks', views.BookEditionPaperbackViewSet)

router.register(r'tv/shows', views.TvShowViewSet)
router.register(r'tv/seasons', views.TvSeasonViewSet)
router.register(r'tv/episodes', views.TvEpisodeViewSet)


router.register(r'external/sources', views.ExternalSourceViewSet)
router.register(r'external/references', views.ExternalReferenceViewSet)

router.register(r'party/persons', views.PersonViewSet)
# router.register(r'contributors', views.ContributorViewSet)
router.register(r'party/companies', views.CompanyViewSet)
# router.register(r'things/places', views.PlaceViewSet)

#router.register(r'things/creative_works', views.CreativeWorkViewSet)
#router.register(r'things', views.ThingViewSet)

urlpatterns = [
    path('api/', include(router.urls)),

    path('shows/', views.TvShowListView.as_view(), name="view-shows-list"),
]
