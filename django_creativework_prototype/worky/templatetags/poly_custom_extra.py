from django import template


register = template.Library()


@register.filter
def to_verbose_class_name(instance):
    return instance._meta.verbose_name


@register.filter
def to_verbose_class_name_plural(instance):
    return instance._meta.verbose_name_plural


@register.filter
def to_class_name(value):
    return value.__class__.__name__
