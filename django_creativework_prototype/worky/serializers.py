from django.contrib.auth.models import User, Group
from rest_framework import serializers

from . import models



class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']


class CompanySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Company
        efields = ["url","name"]


class PersonSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Person
        fields = ["url","name"]


class ExternalSourceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.ExternalSource
        fields = ["url","name"]


class ExternalReferenceSerializer(serializers.HyperlinkedModelSerializer):
    source = ExternalSourceSerializer(many=False)
    class Meta:
        model = models.ExternalReference
        fields = ["url","id", "source"]


class TvSeasonInlineSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.TvSeason
        fields = ["url","name"]

"""class ReferableSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.ExternalReference
        fields = '__all__'


class ReferableeRelatedField(serializers.RelatedField):
    def to_representation(self, value):
        serializer = ReferableSerializer(value.get_queryset()[0])
        return serializer.data"""


class TvShowSerializer(serializers.HyperlinkedModelSerializer):
    seasons_eager = TvSeasonInlineSerializer(many=True, read_only=True, source='seasons')
    seasons = serializers.HyperlinkedIdentityField(many=False, read_only=True, view_name='tvshow-seasons')

    references = ExternalReferenceSerializer(many=True)

    class Meta:
        model = models.TvShow
        fields = ["url", "name", "seasons_eager", "seasons", "references"]

    def create(self, validated_data):
        seasons_eager = validated_data.pop("seasons_eager", [])
        seasons_link = validated_data.pop("seasons", None)
        references = validated_data.pop("references", [])

        instance = models.TvShow.objects.create(**validated_data)
        # TODO: Add references to created isntance. Example below
        """for ref in references:
            # In this creaate case this wil only create new refs, 
            #   but when updating instance then this has to decide if it should update or create new ref
            instance.add_reference(ref)  # Send in dif data. Ref obj, or source and name param.
        instance.save_references()
        # Or
        if instance.references_added():
            instance.save()
        # or save done in each add_reference call?"""

        return instance

    def update(self, instance, validated_data):
        seasons_eager = validated_data.pop("seasons_eager", [])
        seasons_link = validated_data.pop("seasons", None)
        references = validated_data.pop("references", [])
        # TODO: want to be able to edit references... Put should probably be able to delete while patch only change and create
        return super().update(instance=instance, validated_data=validated_data)



class TvSeasonSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.TvSeason
        fields = ["url","name"]


class TvEpisodeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.TvEpisode
        fields = ["url","name"]