from django.shortcuts import render
from django.views.generic import ListView, DetailView
from django.db.models import OuterRef, Subquery, Prefetch
from django.contrib.auth.models import User, Group
from rest_framework import viewsets
from rest_framework import permissions
from rest_framework.decorators import action
from rest_framework import mixins
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from .serializers import UserSerializer, GroupSerializer  # , BookThingSerializer, PersonThingSerializer
from . import serializers
from . import models


def _return_list_view(parent_view, serializer, queryset, request):
    queryset = parent_view.filter_queryset(queryset)
    page = parent_view.paginate_queryset(queryset)
    if page is not None:
        serializer_instance = serializer(page, many=True, context={'request': request})
        return parent_view.get_paginated_response(serializer_instance.data)

    serializer_instance = serializer(queryset, many=True, context={'request': request})
    return Response(serializer_instance.data)


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]

    @action(detail=True, methods=['get'])
    def groups(self, request, pk=None):
        groups = User.objects.get(id=pk).groups.all()
        return _return_list_view(self, queryset=groups, request=request, serializer=serializers.GroupSerializer)


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated]


class TvShowViewSet(viewsets.ModelViewSet):
    # TODO: queryset structure is reused multiple times.
    # TODO: Add common prefetch_related and select_related setup in custom manager code?
    queryset = (
        models.TvShow.objects
            .prefetch_related(
            Prefetch("references",
                     queryset=models.ExternalReference.objects.select_related("source")))
            .prefetch_related(Prefetch("season_set",
                                       queryset=models.TvSeason.objects
                                       .prefetch_related(Prefetch("episode_set",
                                                                  queryset=models.TvEpisode.objects.all()
                                                                  ))
                                       .prefetch_related(Prefetch("references",
                                                                  queryset=models.ExternalReference.objects.select_related(
                                                                      "source")))
                                       ),
                              )
    )
    serializer_class = serializers.TvShowSerializer
    permission_classes = [permissions.IsAuthenticated]

    @action(detail=True, methods=['get'])
    def seasons(self, request, pk=None):
        queryset = models.TvSeason.objects.filter(show=pk)
        return _return_list_view(self, queryset=queryset, request=request,
                                 serializer=serializers.TvSeasonSerializer)


class TvSeasonViewSet(viewsets.ModelViewSet):
    queryset = models.TvSeason.objects.all()
    serializer_class = serializers.TvSeasonSerializer
    permission_classes = [permissions.IsAuthenticated]


class TvEpisodeViewSet(viewsets.ModelViewSet):
    queryset = models.TvEpisode.objects.all()
    serializer_class = serializers.TvEpisodeSerializer
    permission_classes = [permissions.IsAuthenticated]


class ExternalSourceViewSet(viewsets.ModelViewSet):
    queryset = models.ExternalSource.objects.all()
    serializer_class = serializers.ExternalSourceSerializer
    permission_classes = [permissions.IsAuthenticated]


class ExternalReferenceViewSet(viewsets.ModelViewSet):
    queryset = models.ExternalReference.objects.all()
    serializer_class = serializers.ExternalReferenceSerializer
    permission_classes = [permissions.IsAuthenticated]


class PersonViewSet(viewsets.ModelViewSet):
    queryset = models.Person.objects.all()
    serializer_class = serializers.PersonSerializer
    permission_classes = [permissions.IsAuthenticated]


class CompanyViewSet(viewsets.ModelViewSet):
    queryset = models.Company.objects.all()
    serializer_class = serializers.CompanySerializer
    permission_classes = [permissions.IsAuthenticated]



class TvShowListView(ListView):
    template_name = "worky/tvshow_list.html"
    model = models.TvShow
    queryset = (
        models.TvShow.objects
            .prefetch_related(
                Prefetch("references",
                     queryset=models.ExternalReference.objects.select_related("source")))
            .prefetch_related(Prefetch("season_set",
                                       queryset=models.TvSeason.objects
                                       .prefetch_related(Prefetch("episode_set",
                                                                  queryset=models.TvEpisode.objects.all()
                                                                  ))
                                       .prefetch_related(Prefetch("references",
                     queryset=models.ExternalReference.objects.select_related("source")))
                                       ),
                              )
    )
